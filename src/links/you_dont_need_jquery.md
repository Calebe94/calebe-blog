---
title: "Você não precisa de jQuery!"
subtitle: ""
date: "2021-05-18 18:29:13"
tags: []

---

# You really don't need it

* [Github - You Don't Need jQuery](https://github.com/you-dont-need-x/you-dont-need-jquery)
* [You might not need jQuery](http://youmightnotneedjquery.com/)
