<!--
title: "Links úteis para sobre Docker"
subtitle: "Aprendendo e manuseando containers docker"
date: "2021-04-17 18:04:26"
tags: []
-->

## How to Docker

Aqui eu guardo alguns links interessantes para começar a entender o docker.

* [How to Create a Docker Container using Dockerfile](https://hostpresto.com/community/tutorials/how-to-create-a-docker-container-using-dockerfile/)
* [How to list containers in Docker](https://stackoverflow.com/questions/16840409/how-to-list-containers-in-docker)
* [Docker - File](https://www.tutorialspoint.com/docker/docker_file.htm)
* [IDF Docker Image](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/tools/idf-docker-image.html)
* [Build and run your image](https://docs.docker.com/get-started/part2/)
