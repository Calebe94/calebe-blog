<!-- 
title: "Awesome i3wm"
subtitle: "Links úteis sobre i3wm"
date: "2021-04-17 18:04:26"
tags: []
-->

# i3wm Awesome

Alguns links interessantes a respeito de **i3wm**.

## i3 Scripts

### Shell

* [i3ass - i3 assistance scripts](https://github.com/budlabs/i3ass)

### Python

* [3-easyfocus](https://github.com/cornerman/i3-easyfocus)
* [i3-workspace-names-daemon](https://github.com/cboddy/i3-workspace-names-daemon)
* [i3ipc-python](https://github.com/acrisci/i3ipc-python)
* [i3msg-python](https://github.com/Ceryn/i3msg-python)

## dmenu Scripts 

### Shell 

* [dmenu_scripts collection](https://github.com/jukil/dmenu-scripts-collection)

### Python 

* [i3wm's Scratchpad](https://github.com/brantsch/i3-scratchpad-dmenu)


## dotfiles

* [Arrakis i3 style](https://github.com/p0n3/arrakis_i3)

