<!--
title: "pass"
subtitle: "the standard unix password manager"
date: "2021-02-11 18:04:26"
tags: []
-->

# Pass

* [Manage Passwords With GPG, The Command Line, And Pass](https://www.thepolyglotdeveloper.com/2018/12/manage-passwords-gpg-command-line-pass/)
* [How-To: Import/Export GPG key pair](https://www.debuntu.org/how-to-importexport-gpg-key-pair/)

