---
title: "Install Debian on MacBook PRO"
subtitle: ""
date: "2020-09-29 10:34:54"
tags: []

---

## Tutoriais

### Swap

* [Debian - How to use hibernation without a swap partition.](https://wiki.debian.org/Hibernation/Hibernate_Without_Swap_Partition)
* [Dev Connected - How To Add Swap Space on Debian 10 Buster](https://devconnected.com/how-to-add-swap-space-on-debian-10-buster/)
* [Digital Ocean - How To Add Swap Space on Debian 10](https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-debian-10)
* [Linuxize - How to Add Swap Space on Debian 9](https://linuxize.com/post/how-to-add-swap-space-on-debian-9/)
* [Github - Kretcheu](https://github.com/kretcheu/dicas)

### Installing

* [Installing Debian on a old MacBook Pro](https://pvital.wordpress.com/2020/04/27/installing-debian-on-a-old-macbook-pro/)

### Partitions

* [Ubuntu - Partitioning/Home/Moving](https://help.ubuntu.com/community/Partitioning/Home/Moving)

### WebCam

* [Ask Ubuntu - Camera not working on Macbook Pro](https://askubuntu.com/a/991286)

