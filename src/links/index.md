<!--
title: "Links úteis"
date: "2022-02-05 18:04:26"
tags: []
-->

# Links úteis

* [Links úteis para sobre Docker](docker.html)
* [Awesome i3wm](i3-awesome.html)
* [Install Debian on MacBook PRO](installation_on_mac.html)
* [pass](pass.html)
* [Você não precisa de jQuery!](you_dont_need_jquery.html)
* [Bash on Steroids](https://github.com/tinoschroeter/bash_on_steroids) - Use `bash` scripts para escrever aplicações web
