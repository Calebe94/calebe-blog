<!--
title: "Início"
date: "2022-02-05 18:04:26"
tags: []
-->

<center>
## 👋 Olá

### Bem-vindo(a) ao meu blog!
<img src="https://github.com/Calebe94.png" width="200px" height="200px" title="Eu em low poly mode!" alt="Eu em low poly mode!" class="round-image">
</center>

Meu nome é **Edimar Calebe Castanho**, sou formado em Engenharia da Computação e sou um desenvolvedor de software. Tenho experiência em sistemas embarcados e programação de baixo nível. Sou apaixonado por explorar novas tecnologias e compartilhar meu conhecimento com outras pessoas.

<center>
<div class="retro-no-decoration">
📋 Currículo: [🇺🇸   inglês](https://github.com/Calebe94/resume/releases/latest/download/curriculum.pdf) - [🇧🇷     português](https://github.com/Calebe94/resume/releases/latest/download/curriculum-pt.pdf)
</div>
</center>

Neste blog, você encontrará uma variedade de tópicos relacionados ao mundo da tecnologia, programação e desenvolvimento de software. Aqui, estou empenhado em fornecer conteúdo útil, tutoriais, dicas e truques, além de compartilhar minhas experiências e projetos pessoais.

Durante minha jornada profissional, trabalhei em projetos de sistemas embarcados utilizando linguagens como C e Python, bem como os sistemas operacionais Linux e FreeRTOS. Tenho experiência no desenvolvimento de aplicativos utilizando Flutter, Flask e tecnologias web como HTML, CSS e JavaScript.

<center>
<img src="https://github-readme-stats.vercel.app/api/top-langs/?username=calebe94&theme=dark&hide_progress=true" title="Top Langs" alt="Top Langs">
</center>

Além disso, sou apaixonado por práticas de DevOps, incluindo integração contínua, entrega contínua e automação de processos de desenvolvimento de software. Tenho habilidades em ferramentas e plataformas como Git, Jira, Github, Gitlab, Docker e SDL2.

Este blog é meu espaço para compartilhar meu conhecimento, insights e opiniões sobre tecnologia e desenvolvimento de software. Meu objetivo é ajudar a comunidade de desenvolvedores, estudantes e entusiastas de tecnologia a aprender e crescer juntos.

Fique à vontade para explorar o conteúdo disponível aqui e não hesite em entrar em contato comigo caso tenha alguma pergunta, sugestão ou apenas queira trocar ideias. Você também pode me encontrar nas redes sociais, como [LinkedIn] e [GitHub], onde compartilho atualizações e projetos em que estou trabalhando.

Obrigado por visitar meu blog e espero que você encontre informações valiosas e inspiração aqui!

## Sobre este site

Este é um site estático gerado a partir de **Markdown** usando [pandoc](https://pandoc.org/) com meu Shell Script POSIX baseado no [ssg](https://www.romanzolotarev.com/ssg.html).
O site é desenvolvido para ser leve e acessível, respeitando a privacidade de quem o acessa.
O código-fonte do site está disponível no [gitlab](https://gitlab.com/Calebe94/calebe-blog/).

[LinkedIn]: https://www.linkedin.com/in/calebe94/
[GitHub]: https://github.com/Calebe94
