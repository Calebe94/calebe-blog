<!--
title: "Minhas recomendações"
subtitle: ""
date: "2023-06-03 11:47:26"
tags: []

-->

# Meus Favoritos

<a rel="noopener noreferrer" class="card-link" href="./channels.html">
<div class="card">
<img src="https://1.bp.blogspot.com/-hY5-pNrOcKw/XeI_00cpCgI/AAAAAAAAF4A/J7jS49V8kNozycy0PgY6wfc7SUU9gulTgCLcBGAsYHQ/s1600/Youtube-Icon-square-2340x2340.png" class="card-icon">
<div class="card-content">
<div class="card-title">Canais de Youtube</div>
<div class="card-description">Os canais de youtube que eu gosto e recomendo</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./artists.html">
<div class="card">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Spotify_App_Logo.svg/1200px-Spotify_App_Logo.svg.png" class="card-icon">
<div class="card-content">
<div class="card-title">Artistas / Bandas</div>
<div class="card-description">Os artistas e bandas que eu gosto e recomendo</div>
</div>
</div>
</a>
