<!--
title: "Canais de Youtube"
subtitle: ""
date: "2023-08-20"
tags: []

-->
## Canais de Youtube\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@Normose_">
<div class="card">
<img src="https://yt3.googleusercontent.com/IYhw8vOo4wlYg8_SGSnz4nbFXUnUrnig1xziAIYPuriIOKsmZgCgLnoQrjwVeb_0qjTubsyz=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">NORMOSE</div>
<div class="card-description">Normose é um canal de brainstorms para quem busca reflexão e conhecimento crítico: História, Filosofia, Divulgação científica, Política, Arte e Estética.   Inscreva-se e fique suaaave! </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@HistoriaCabeluda">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaSOZhKcf1OcXr-hoNSuY3uRdWWfyBrt97v_wrznyA=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">História Cabeluda</div>
<div class="card-description">Canal que produz e publica materiais voltados para a educação política dos trabalhadores para construir a Revolução Brasileira.    </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@assimdisseojoao">
<div class="card">
<img src="https://yt3.googleusercontent.com/BKEaytxQ---YKwmiZlY0ao2vqcPkQ1q5PsLJYCSrm0wiuUtlulXwG9PURyQOQ-WmJQ5qq-IYfA=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">João Carvalho</div>
<div class="card-description">Historiador e militante comunista. MLM.  Youtuber e Streamer no /assimdisseojoao Membro do coletivo Soberana Rebelar-se é justo!  Conheça também o canal de cortes! https://youtube.com/CortesdoJoao  Contato comercial: joaocarvalho@nmatalents.com </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@CortesdoJoao">
<div class="card">
<img src="https://yt3.googleusercontent.com/a_f6tIlTd_3HXBHvYRlznj1IE22y9oVzPgY7CN5r43zBXPbPjpM7PJBcwfoTdp-wD4xGAKcJeh8=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Cortes do João Carvalho [OFICIAL]</div>
<div class="card-description">CANAL OFICIAL de CORTES do João Carvalho, que é historiador, mestre, doutorando e professor, dentre outras tantas coisas!  Conheça e inscreva-se no canal principal em youtube.com/assimdisseojoao  Siga o João em todas as redes procurando pela @ "assimdisseojoao" (instagram, twitter, tiktok, twitch)  Contribua com nossa produção de conteúdo em: - apoia.se/assimdisseojoao  - padrim.com.br/assimdisseojoao Você também pode ser membro(a) do Clube de Canais aqui no YouTube! Acesse youtube.com/cortesdojoao/join  Envie sua SUGESTÃO DE CORTE em bit.ly/cortesdojoao  Artes gráficas e capas (vitrines/thumbs) por Manoela "Nunih" Bertelli. Conheça o trabalho dela em: - instagram.com/nunihsart - tiktok.com/@nunihsart - e tudo mais em nunih.com  Gestão do canal, posts na aba comunidade, edição de vídeo, pós-produção e SEO por Rapha Corrêa. Conheça o trabalho em: - instagram.com/fotosdorapha - youtube.com/@RaphaCorrea - twitch.tv/FotosDoRapha - e nas outras redes via raphaelcorrea.net </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@HistoriaPublicaOficial">
<div class="card">
<img src="https://yt3.googleusercontent.com/UHgloy_IOHbHhMylgl4WXpWMpRq0ZzIVVCkoog4D54FQBf6gfUbEGQ4sceeFi1STqQZ0WK5R1g=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">História Pública</div>
<div class="card-description">Um canal dedicado à informação, ao debate e à análise política feito por historiadores marxistas.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@CortesdoHistoriaPublica">
<div class="card">
<img src="https://yt3.googleusercontent.com/SYQFta0g7uPasKTgy7SZG2cFFJUI99yWOyHO9eTR6ojZgMDncg6SXr0fS5LXFJnu9ZmIi1zeUxg=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Cortes do História Pública</div>
<div class="card-description">Canal de cortes oficial do História Pública.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@ArquivoHistoriaPublica">
<div class="card">
<img src="https://yt3.googleusercontent.com/c6B9iE9XA7XI57Znaf7ur2yPpA71fM1JdU8QEoCmeaR8lV-xWejf7_P0Rrjs0u7rfChsaFLxCQ=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Arquivo História Pública (Não Oficial)</div>
<div class="card-description">Este é um canal NÃO OFICIAL! Aqui deixo salvo as lives do Ian (História Pública) Canal oficial do Ian: @historiapublica    </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@TemperoDrag">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaRYOqG3Jt9PpTxMCpcLRch7_13GOWh6FIiTnXmk_A=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Tempero Drag</div>
<div class="card-description">Desde 2015 tratamos de temas sociais e políticos com humor e arte.  Acreditamos na educação como ferramenta de emancipação e trabalhamos em união por mais e melhores acessos.  Venha provar nosso Tempero Drag :)</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@oalgoritmodaimagem">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaT9K--T9hF7VtfEvt337aFIR9D0PjNBmCnjySdo=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">oalgoritmodaimagem</div>
<div class="card-description">Análises semióticas e sociológicas sobre celebridades, políticos e empresas.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@HumbertoMatos">
<div class="card">
<img src="https://yt3.googleusercontent.com/3uPBiwFfVQpcs0RVpn5n5TtjrxtqeuHEDHpc32kKVaStnXkDo5d2PzcdVHz9YmVSFqkoEb4h=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Humberto Matos</div>
<div class="card-description">Canal de críticas sobre economia política com pitadas de história, filosofia e sociologia.  Seja um dos nossos apoiadores e ajude a manter o Canal:  Nosso site com todas as nossas plataformas de apoio: (Apoia-se, Catarse, PayPal, Depósito via TED ou TEV)  https://saiadamatrix.github.io</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@JonesManoel">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaRVVuVe6K3n1_NKAmD0r4ifA3xMxIok0yKBq6lk=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Jones Manoel</div>
<div class="card-description"></div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@TeseOnze">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaTKaacmymnUErjLmacYdUFMlv5BvbP4Ff35l5DF=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Tese Onze</div>
<div class="card-description">O Tese Onze é um canal focado em apresentar contrapontos ao senso comum, trazer análises sobre sociologia e política, e acumular bagagem pra transformar o mundo. Apesar do conteúdo ser embasado em pesquisa, não se trata de um canal preparatório de conteúdo educacional, mas de informação e formação política com forte viés marxista.  O canal é produzido e apresentado por Sabrina Fernandes, doutora em sociologia, feminista marxista, ecossocialista e vegana. É autora do livro "Sintomas Mórbidos: a encruzilhada da esquerda brasileira" e "Se quiser mudar o mundo: um guia político para quem se importa".   Vídeos semanais, geralmente às quintas-feiras.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@mimimidias">
<div class="card">
<img src="https://yt3.googleusercontent.com/I31JLL0ALC_M0e3EMRYL6W7Vnt5a9o8dmv7jcI3kvJeT2ZCOJX9wyCTQP3aL6fBwHGQrlsX7Rg=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">mimimidias</div>
<div class="card-description">Vídeos sobre internet, cinema, games, design, literatura e tudo que é mídia. Sempre com rigor acadêmico, bom humor e muito mimimi. Somos estudiosos e acadêmicos apaixonados por mídia e nossa vontade de conversar sobre o assunto transbordou as páginas de artigos e as salas de aula.  Clara Matheus é doutoranda em Estudos Literários, roteirista freelancer e pesquisadora. Leonardo de Oliveira é Designer UX, mestre em Design, bacharel em Design Gráfico e em Artes Visuais.  Tavos Mata Machado é doutorando em Teoria da Literatura, tradutor e professor de inglês.  O SVBR - ScienceVlogs Brasil -  é um selo que atesta qualidade da divulgação científica no Youtube. Somos um Parceiro SVBR, prezamos pela boa divulgação científica e buscamos com vigor fortalecer o SVBR, assim fomentando a comunidade diversa de divulgadores científicos no YouTube. http://www.youtube.com/c/ScienceVlogsBrasil</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@FelipeDurante">
<div class="card">
<img src="https://yt3.googleusercontent.com/DQxmeBMmm8NmQC288lW9a8N0um2aMXL3E-_foYBzNuF4cTrd48cBg5iZsiWS9XN9yLS4LpfpYw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Felipe Durante</div>
<div class="card-description">Não adianta me mandar pra China. </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@ChavosodaUSP">
<div class="card">
<img src="https://yt3.googleusercontent.com/Y0S46vFCCx-v8VabRJosyE0Dp48QeJQyDhQsrf4jAzT0jMqSqN40FxT8NeK-H_VvI1UgmYsY=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Chavoso da USP</div>
<div class="card-description">"Injetar ódio no cérebro do conformado, informação no desinformado e autoestima no derrotado". — Facção Central</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@mylaura_m">
<div class="card">
<img src="https://yt3.googleusercontent.com/jnlyjjGQZc4fToaKT4ZAbsmik8JKNvxw7fza_L6gvYnxdzNkKU0qEH-DLoLs4ivr9E7iu_outxc=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Laura Sabino</div>
<div class="card-description"></div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@MeteoroBrasil">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaTx0By2Ano6G8FsGKQHNx0Gx2iX1NATK9TD64ABMA=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Meteoro Brasil</div>
<div class="card-description">Meteoro é um canal sobre cultura pop, ciência e filosofia. E esse humilde canalzinho acaba de ficar mais alegre e completo por um bom motivo: você o encontrou. Muito obrigado!  Caixa Postal:17905 CEP: 80410-981</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@SNSalysson">
<div class="card">
<img src="https://yt3.googleusercontent.com/l9sWUSpWRBtdkpNY41LzWFCGLpU7ZhOqKMvk06o1q9kRuIDDZgOKm9XmVUWk9xbO4ETli8vaWw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Alysson Mascaro</div>
<div class="card-description"></div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@loadcomics">
<div class="card">
<img src="" class="card-icon">
<div class="card-content">
<div class="card-title"></div>
<div class="card-description"></div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@TiagoSantineli">
<div class="card">
<img src="https://yt3.googleusercontent.com/HJmedQdsuwUIBtXnl7bjMN2UmBshBy0vP4TfdVBtxtuzuEzoW9c4IRGQaMXRI0s8KXT78yxJfw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Tiago Santineli</div>
<div class="card-description">Só se inscreve aí e pronto carai.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@carrodereview">
<div class="card">
<img src="https://yt3.googleusercontent.com/yhmVRfBVF6EJ5ljpvQlM2Mw-34MlMkTnYP1wMN5TFnN_l1DV2bRIc34KvtinZHm3taf1N64PNw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">carro de review</div>
<div class="card-description">review de carros comuns para pessoas comuns </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@CentelhaVermelha1917">
<div class="card">
<img src="https://yt3.googleusercontent.com/0iskFHFz4GuN0J6GUmJ47Yvi6ht1f51-b7oshhzSp6kTIfBBmB61ZynPLFmKu81e3Xa4RZAF=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Centelha Vermelha</div>
<div class="card-description">Um canal dedicado ao fortalecimento dos nossos camaradas, cortes, vídeos, traduções. Siga os canais dos camaradas pelo link na descrição de cada vídeo ou pelas recomendações do canal. Caso queira que eu retire um corte seu, mande um e-mail ou mensagem em uma das redes sociais Venceremos!   A música no final dos vídeos foi feita pelo Gui Selfsounds, sigam o canal e o tiktok do camarada O banner foi feito pela Groselha Atômica </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@FerrezOficial">
<div class="card">
<img src="https://yt3.googleusercontent.com/FWfmQAjMppqWyWAUhDbIpyUOJmkNXsTnb0jKREs8Nj7fr0R8TNhbjMaqpC7k8-1cESM2T9D3gA=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">FERRÉZ</div>
<div class="card-description">Ferréz é o meu pseudônimo, lancei o primeiro livro em 1997,  Tem sido uma correria espalhar literatura por tantas periferias do Brasil.   Venho produzindo a 25 anos: textos, palestras e vídeos  sobre cultura da periferia e todo esse conteúdo sempre foi disponibilizado no meu blog e no facebook. Além disso rapa, todo esse trabalho é próprio  e tudo que crio e produzo tem a visão de formar novos leitores e trazer o senso crítico pro nosso povo e pra todos que simpatizam com a causa.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@radioguerrilhaoficial">
<div class="card">
<img src="https://yt3.googleusercontent.com/PnkLsE7BygebDmP7UZkxTuWK6c-1ewbfpG3I9MAPG5z4KYVTXdkOw7k-3MTl1PW7Yp99KvAT=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Rádio Guerrilha</div>
<div class="card-description">Canal oficial da Rádio Guerrilha!  Para mais informações acessem: twitch.tv/radioguerrilha </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@luideverso">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaRyRYGK6GyhjZ7qZhlLg0uEzIUhPeMgah1_2_ytZw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">luideverso</div>
<div class="card-description">Olá, bem vindo ao canal de CORTES DO LUIDEVERSO. Me chamo Luide Matos e produzo conteúdo há 16 anos.  Algumas informações importantes: - É um canal de react, só isso mesmo. - As lives acontecem na Twitch de segunda à sexta feira, sempre a partir das 11h da manhã. - Sou criador do FORMANDO CREATORS, te ensino a criar e viver de conteúdo.  Assine agora e acesse nossa formação contínua por R$ 24,90 ao mês: https://hotm.art/formandocreators </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@ResistenciaContemporanea">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaQAC43rQFOaLjHGGlzNHfIFuhzE2cooR7PFNK6V-w=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">TV Resistência Contemporânea</div>
<div class="card-description">A TV Resistência Contemporânea é um canal de comunicação popular, alternativo, independente, coletivo, progressista e plural. Com o objetivo de levar aos seus seguidores reflexões sobre temas importantes e necessários da Política e da Ciência Política, História do Brasil e Mundial além de temas como Cidadania, Políticas Públicas, Direitos Humanos e Democracia.  Nossa razão existencial: A organização política da Classe Trabalhadora do Brasil e do Mundo, como caminho para alcançarmos o socialismo.  OBSERVAÇÃO IMPORTANTE: Os temas das lives levadas ao ar não representam, necessariamente, a opinião da TV Resistência Contemporânea e é de responsabilidade do/a colunista.  Gilbert Martins - Editor Responsável - @ArrudaGilbert Mateus Martins - Editor Givalber Arruda - Editor     </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@CarollineSarda">
<div class="card">
<img src="https://yt3.googleusercontent.com/S2IuTf7EA3XXNadiqSxXCfhSA9IOPCOcHIw85Oh3r54drQvMBbS6N-ipe2zzJziou0DnktzF=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Carolline Sardá</div>
<div class="card-description">Conto histórias de mulheres, explico sobre direitos feministas e faço alguns reacts radioativos nas horas vagas.  </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@GalasFeios">
<div class="card">
<img src="https://yt3.googleusercontent.com/C1a-DsdWODwXLFIn5gOMIcHEBRdDZOnzeK2YXVCr_hmcqBELiHZoBHULP2m1bjVkwhx9o46oAw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Galãs Feios</div>
<div class="card-description">Humor e opinião sobre política e cultura pop em geral pelos amigos Helder Maldonado e Marco Bezzi, que destilam seu ódio festivo pra falar também de música, cinema, TV e celebridades. A dupla que mais denuncia os golpes apresenta Lives e vídeos diários.   Já falamos com Julinho da Van do Choque de Cultura, Ciro Gomes, Hermes e Renato, Randolfe Rodrigues, Lucas Jagger, João Gordo, Gregorio Duvivier, Reinaldo Azevedo, Gastão da MTV, Suplicy, Diva Depressão etc.  A página do Facebook e o conceito da Galãs Feios foram criados pelo jornalista Helder Maldonado (Rede Record – R7) em abril de 2016.  No final do mesmo ano, o jornalista e escritor Marco Bezzi  (Estadão, Ed. Abril, Rede Record) entrou como parceiro para ativar outras redes sociais como o Instagram, YouTube, website e eventos em geral.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@FisicaeAfins">
<div class="card">
<img src="https://yt3.googleusercontent.com/sqhoPoHqgKHJ7qYGDinv3zkDXmxXArPQ7cNBhq3K4k3oaPFt-B2AlA7_tVedkZumxNaN3YNpzQ=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Física e Afins</div>
<div class="card-description">Gabriela Bailas é PhD em Física Teórica de Partículas e conclui seu doutorado na França através de uma bolsa do governo francês. Atualmente está concluindo uma especialização em Neurociência e Comportamento. É professora, pesquisadora, autora e comunicadora. Aqui compartilho informações sobre Ciência. Disseminadora do pensamento crítico e do ceticismo.   Todos os produtos do Física e Afins em www.fisicaeafins.com.br  Quer me enviar algum presentinho, cartinha e/ou docinhos? CX POSTAL: 2002  CEP: 96015-145  Pelotas/RS  Quer uma parceria conosco? contato.fisicaeafins@gmail.com Quer ADQUIRIR O PLANNER EXCLUSIVO DO FÍSICA E AFINS? www.instagram.com/bibibailas  Quer me seguir em tempo real? Instagram: www.instagram.com/bibibailas Twitter: www.twitter.com/bibibailas Facebook: Física e Afins   Parceiros do Física e Afins: www.goodvibres.com ~ cupom "BIBI10" www.insiderstore.com.br  ~ cupom "FISICAEAFINS12" </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@OraThiago">
<div class="card">
<img src="https://yt3.googleusercontent.com/ut36YsngUnK7qGyxN_1jEgMUy5glFzQM5-5ugvUMjY4Tj3nW9fWLyFZcyNpjplzrzTZF6oWp=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Ora Thiago</div>
<div class="card-description">Meu nome é Thiago, eu faço videos sobre cultura pop e comportamento.   Apoie o canal: https://apoia.se/orathiago CONTATO/COMERCIAL: orathiago@brunch.ag Instagram: https://www.instagram.com/orathiago/ Twitter. https://twitter.com/orathiago</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@MarxComenta">
<div class="card">
<img src="https://yt3.googleusercontent.com/iYLf3MQQWTcU5U4i_Yp12KjAzDAYx02nkMNGb6EBGUZy7KIHu5MNpIq9zfyHbg97bTopEAXt_g=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Marx Comenta Canal</div>
<div class="card-description">Marx voltou e no Rio de Janeiro! Aqui é conhecido como Karlos Marx ou "barbudo". Trabalhadores do mundo uni-vos ou vamos junto e misturado, rapaziada!</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@historiadaimagem">
<div class="card">
<img src="https://yt3.googleusercontent.com/TcMpLe3S0xZLJDsWtzGMgsegLvO7kIoOwKp6wJJUlrq1F0zHU_gY1lPOX01cHWR5mbGhx7tlRQ=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">História da Imagem</div>
<div class="card-description">Oi! Eu sou a Isa, e aqui falamos sobre História, Cultura e Arte!   </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@SovietLunox">
<div class="card">
<img src="https://yt3.googleusercontent.com/wgz7CHhCkY14BsNTvi6Dcr8qkDFti0yq8zPAFnOT9r7rC5mW3NtT9QDTFPFIk6t-jjbxG4P0=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">SovietLunox</div>
<div class="card-description">Oi! sou uma streamer comunista que produz musica e organiza a revolução nos joguinhos!</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@dudabolche3466">
<div class="card">
<img src="https://yt3.googleusercontent.com/WjSTrHGZ-WB3Fz1vm50byzO_5o7nGVUqrrpZG7pYjHbq67SKgjpI0R4zPQ8ssAJxzgiZPXU17g=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Dudabolche</div>
<div class="card-description"></div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@Lunaoi">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaSOAdyd-3sa5fGFvFzhezU6N7DHLMN9rW-uiDTC=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Luna oi!</div>
<div class="card-description">Hi everybody! I&#39;m Luna, welcome to Luna Oi! Every week we bring you videos and live streams about culture, history, and politics in Vietnam, as well as panels and interviews with indigenous activists and comrades in the Global South.  Luna Oi! is a proud member of the Non-Compete content collective! See our other channels at: https://www.non-compete.com/  Follow me on Twitter at: https://www.twitter.com/lunaoi_vn/ </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@LadyIzdihar">
<div class="card">
<img src="https://yt3.googleusercontent.com/jgSu_xaznaUccoY1LwdHQLR3a2EefAxi6p5QCW_aW5aiXxbxIvQqISJ-FcN_5Injqo32jjz9=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Lady Izdihar</div>
<div class="card-description">Asalam alaikum comrades! I&#39;m a historian on Russian history but mostly focus on the first 30-40 years and the cultural and ethnographic makeup of what the USSR was.  I&#39;m all about humanization and covering the positive aspects of Soviet &amp; Russian history as that&#39;s what&#39;s missing in the English language, the lesser known, misunderstood and misrepresented.  *I also love Vexillology and political geography 🚩🗺️ </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@JuFurno">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaR6jbjjuYm7KbsJ74aINhafSXXT2V6vLClxS1Qshw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Ju Furno</div>
<div class="card-description">@JuFurno é uma mulher jovem. É gaúcha, torce para o colorado mas vive em sampa. É mestre e doutora em desenvolvimento econômico no instituto de economia da unicamp (AQUELA universidade no interior de SP que liberais costumam não gostar) Além disso é militante de esquerda e assídua leitora de literatura</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@monivilela">
<div class="card">
<img src="https://yt3.googleusercontent.com/GBPetdANo67-nBNz8hnsCfmflkn2kAqBWqF8M-4uUJAORWUZtAapehPcuuZadtn6ryeZTwSZAw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Canal Monique Vilela</div>
<div class="card-description">Notícias e bastidores do dia a dia do Athletico</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@TretisOficial">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaQnp7bSVXTVTcfFkAzXBGnutUQz6QbKg1KTNwmy=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Trétis</div>
<div class="card-description">A Trétis é um portal independente que traz notícias em primeira mão do Athletico Paranaense.  É mais que oficial, é Trétis.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@3Pontos">
<div class="card">
<img src="https://yt3.googleusercontent.com/nM5RS1lNgYu7HROEvWZOXUIJ8FOA9TTZprr_a0CU6rCXOQlRDI2a4EAVif65DF11ChWBhTCxW5Y=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Três Pontos</div>
<div class="card-description">O Canal 3 Pontos tem três pilares na cobertura do Club Athletico Paranaense: Informação, Opinião e Furacão. Sempre trazendo vídeos e lives falando sobre o maior clube do Futebol Paranaense!   Acompanhando em todas as competições da temporada: Libertadores, Copa do Brasil e Campeonato Brasileiro! </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@oquintovento">
<div class="card">
<img src="https://yt3.googleusercontent.com/3LYOpYaQb9ij90GBS_DZomMB9j-Dc_hLGN8CCz1aQnP_dkHtJKaOuUEjALsDgyHanbXGjzVmeg=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">o quinto vento</div>
<div class="card-description">canal sobre o athletico paranaense.  aqui falamos sobre futebol, design e tudo que permeia o furacão da baixada. vídeos 3 vezes por semana pra você ficar sabendo um pouco sobre o que pensa a torcida do rubro-negro das araucárias.  chegue mais, se inscreva, veja nossos vídeos e ouça os podcasts. </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@MarcRebillet">
<div class="card">
<img src="https://yt3.googleusercontent.com/V6sDLHkVN8yZNeyVHK4YXIJH-63d0zCwsi-YZVV-VzEFmgG-Mwo9XazeLaQCyKfEJ8heLEUTVQ=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Marc Rebillet</div>
<div class="card-description">I hope you enjoy my little channel.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@MarceloBechler1">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaSw_jjQHKR8DfEFjDQqAI8WQ4_4feMFe3bZWhTGiA=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Marcelo Bechler</div>
<div class="card-description">Jornalista esportivo. Correspondente desde 2015 em Barcelona. Aqui para analisar futebol, da forma mais simples possível, para vocês!</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@Boy_Boy">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaR-GoXZmXHtIR5gw41sIKzHu1GWzeiwLDUrH2RK=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Boy Boy</div>
<div class="card-description">two boys</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@debxp">
<div class="card">
<img src="https://yt3.googleusercontent.com/IpMGdulJ9DUs8F8RwRvIPOjwyzcYhBDcgxDOxLWVpXKyhXy-FJkEDJAoTNs7K_bE-Ginr5Wzdw=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">debxp</div>
<div class="card-description">Cursos, dicas e muita escovação de bits no Debian GNU/Linux.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@SaudenaRotina">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaQJje6hFfRI9S-lYcI1HsA_FgDkSA82T6ZYLVwOmQ=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Saúde na Rotina com Diego e Dafne</div>
<div class="card-description">Você tem vontade de cuidar melhor da saúde mas acha chato fazer exercícios ou não tem tempo pra isso?  O Professor de Educação Física pela USP Diego Paladini e a instrutora de yoga Dafne Amaro dão dicas e informações sobre emagrecimento, saúde e exercício de maneira bem humorada, porque acreditamos que todo mundo tem o direito de saber que dá pra ser SAUDÁVEL SEM RADICALISMOS :)  Vídeos novos toda semana!  Contatos publicitários: Mariana | mariana@supernova.etc.br  Compre nosso livro "Dá pra ser saudável sem ser chato": https://amzn.to/3ab7yIO  Conteúdos especiais: https://www.saudenarotina.com  Consulte sempre um médico antes de começar a fazer exercícios. Qualquer atividade física feita sem a supervisão de um Professor de Educação Física competente pode causar lesões. Por isso, sempre frisamos nos vídeos que é preciso ir devagar, ter paciência com os resultados e respeitar os limites do seu corpo.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@TayTraining">
<div class="card">
<img src="https://yt3.googleusercontent.com/ytc/AOPolaRUFFYVvzQNdgXQPD4wK7zFe6yvHMpYam0_CnMjPA=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Tay Training</div>
<div class="card-description">Tenha 1 ano de resultados físicos aparentes em apenas poucos meses de treino. Especializado em resultados físicos para mulheres.  </div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://www.youtube.com/@profphilippeleao/videos">
<div class="card">
<img src="https://yt3.googleusercontent.com/UgO7ltbDWLa_qQZxI72CS_OmOoxNVeC88WlRBDUaDa9qTT8mGOmGjnmB2QwR7wTI9vRQfb7eyb0=s900-c-k-c0x00ffffff-no-rj" class="card-icon">
<div class="card-content">
<div class="card-title">Philippe Leão</div>
<div class="card-description"></div>
</div>
</div>
</a>
\n\n
\n\n
