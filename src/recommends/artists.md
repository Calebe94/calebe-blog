<!--
title: "Artistas / Bandas"
subtitle: ""
date: "2023-08-20"
tags: []

-->
## Artistas\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/6JW8wliOEwaDZ231ZY7cf4">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebbb178dabf142a802276a572d" class="card-icon">
<div class="card-content">
<div class="card-title">Sepultura</div>
<div class="card-description">Artist · 1.6M monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/1co4F2pPNH8JjTutZkmgSm">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb5b9f46a5c5bf8243179d56b0" class="card-icon">
<div class="card-content">
<div class="card-title">Ramones</div>
<div class="card-description">Artist · 6.4M monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/3WPKDlucMsXH6FC1XaclZC">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb34e9707779175a2453a1d786" class="card-icon">
<div class="card-content">
<div class="card-title">Suicidal Tendencies</div>
<div class="card-description">Artist · 903.4K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/4lYtGx5NZQJHsMyhHc5iz3">
<div class="card">
<img src="https://i.scdn.co/image/fc6b731da9ccf21380f2c32a37a825b8b6e41fc8" class="card-icon">
<div class="card-content">
<div class="card-title">The Cramps</div>
<div class="card-description">Artist · 1.9M monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/2vJjvxt06vKyDMhwtjTFie">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebf6dbd0dddbb32ef7acd34664" class="card-icon">
<div class="card-content">
<div class="card-title">Dog Party</div>
<div class="card-description">Artist · 3.1K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/6aVjo0xHSiuW5hkasoYSR3">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb8f5884181b6e9173a1e3e6dd" class="card-icon">
<div class="card-content">
<div class="card-title">Melvins</div>
<div class="card-description">Artist · 576.1K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/0gvHPdYxlU94W7V5MSIlFe">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebfc226caae7dd29541e337a18" class="card-icon">
<div class="card-content">
<div class="card-title">Bikini Kill</div>
<div class="card-description">Artist · 508.1K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/13dTrWNNrnZ3AkgNyQNKP5">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb4a5ba59a0ccb0ad8fa740ba0" class="card-icon">
<div class="card-content">
<div class="card-title">The Linda Lindas</div>
<div class="card-description">Artist · 273.9K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/2d0hyoQ5ynDBnkvAbJKORj">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebda4bd2b213cae330e2a4a901" class="card-icon">
<div class="card-content">
<div class="card-title">Rage Against The Machine</div>
<div class="card-description">Artist · 9.8M monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/0KBREvMNpwoBKFlk7aD0Ye">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebadfb36a843b2ee8c92ffb07d" class="card-icon">
<div class="card-content">
<div class="card-title">Camarada Janderson</div>
<div class="card-description">Artist · 2.4K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/4Aivs91UUqz8KVTPL3Dvac">
<div class="card">
<img src="https://i.scdn.co/image/ab67616d0000b273f91da533fde4efd6b649dbfa" class="card-icon">
<div class="card-content">
<div class="card-title">Camarada Marcelinho</div>
<div class="card-description">Artist · 339 monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/29CQLw9uLWsl8Qkz9holfr">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb43a0d1ae0af71095fcb26726" class="card-icon">
<div class="card-content">
<div class="card-title">Racionais MC&#x27;s</div>
<div class="card-description">Artist · 3.3M monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/6U98XWjrUPnPtPBjEprDmu">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebfd2973d1223ebefb570f32f4" class="card-icon">
<div class="card-content">
<div class="card-title">Don L</div>
<div class="card-description">Artist · 433.1K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/3V3rRpp5sbiBNY34AdnplK">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb06046239325ad0cda8e569d4" class="card-icon">
<div class="card-content">
<div class="card-title">Facção Central</div>
<div class="card-description">Artist · 294.5K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/1PA11Cy0SrjmwymvGS9h0C">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebc3c8bec4798f420bc24e5208" class="card-icon">
<div class="card-content">
<div class="card-title">509-E</div>
<div class="card-description">Artist · 437.4K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/6syQjkQSMIrzw5cFnNRheo">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebdf56f99b90815b6b6954fbc8" class="card-icon">
<div class="card-content">
<div class="card-title">Rincon Sapiência</div>
<div class="card-description">Artist · 510.6K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/7bzxsNuoCLrHXzs34HEfL2">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebb9a80060a59c56cbb2440401" class="card-icon">
<div class="card-content">
<div class="card-title">Juventude Maldita</div>
<div class="card-description">Artist · 6.8K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://soundcloud.com/movimentosemterra">
<div class="card">
<img src="https://i1.sndcdn.com/avatars-n25QaD0bXnA5DDHQ-zkXNug-t500x500.jpg" class="card-icon">
<div class="card-content">
<div class="card-title">Movimento Sem Terra - MST (Oficial)</div>
<div class="card-description">Listen to Movimento Sem Terra - MST (Oficial) | SoundCloud is an audio platform that lets you listen to what you love and share the sounds you create.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/1axNKwnXW10Ncg1dv4dwah">
<div class="card">
<img src="https://i.scdn.co/image/ab67616d0000b273136d67419509df09c9f929bc" class="card-icon">
<div class="card-content">
<div class="card-title">Garotos Podres</div>
<div class="card-description">Artist · 56.1K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/3sNclBHudVfCUPFqsHgvHH">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebfee91308a59e343b15db372a" class="card-icon">
<div class="card-content">
<div class="card-title">Flicts</div>
<div class="card-description">Artist · 11.5K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/3d2xlrGC9JGD7ycsf0e8mF">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebfd18a0e844fb7ea9f03607a7" class="card-icon">
<div class="card-content">
<div class="card-title">Ratos De Porão</div>
<div class="card-description">Artist · 84.4K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/6S6ll1Wa6FYvAgThEBX6qJ">
<div class="card">
<img src="https://i.scdn.co/image/ab67616d0000b273b3c4bf6512f5db84171e93e2" class="card-icon">
<div class="card-content">
<div class="card-title">Cólera</div>
<div class="card-description">Artist · 17.7K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/2WEABapGGzYET6Dq5tmIDi">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb6b12fc76c92bb2e184908c75" class="card-icon">
<div class="card-content">
<div class="card-title">Mukeka di Rato</div>
<div class="card-description">Artist · 23.7K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/4yarsvPMSeP8Xqg165gpFP">
<div class="card">
<img src="https://i.scdn.co/image/ab67616d0000b27321f3a85036a7e630f1f33c52" class="card-icon">
<div class="card-content">
<div class="card-title">Devotos Do Odio</div>
<div class="card-description">Artist · 6.6K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/1BFfeW4Vfd9begOhiwoi1D">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebe3feccec5d49232fcdaea5c6" class="card-icon">
<div class="card-content">
<div class="card-title">Devotos</div>
<div class="card-description">Artist · 3.5K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/27CzKTYN9snef9Hn1kBHRG">
<div class="card">
<img src="https://i.scdn.co/image/ab67616d0000b27384302e7ca13f37ad3a44e767" class="card-icon">
<div class="card-content">
<div class="card-title">Olho Seco</div>
<div class="card-description">Artist · 5.8K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/313wZXuksG2rnaLJz9HMWu">
<div class="card">
<img src="https://i.scdn.co/image/ab67616d0000b273eaaf9a4349b7526f054f753e" class="card-icon">
<div class="card-content">
<div class="card-title">Agrotoxico</div>
<div class="card-description">Artist · 2.8K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/5TM5LOr6mxQGod2sH9Uz9e">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb94a9791ac71ccadf5ebd1f9c" class="card-icon">
<div class="card-content">
<div class="card-title">Surra</div>
<div class="card-description">Artist · 22.3K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/1Px6nQCyIRM4Gj0tyvZ1TU">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb97c50f5ca9a96cade0f74957" class="card-icon">
<div class="card-content">
<div class="card-title">Sick Of It All</div>
<div class="card-description">Artist · 120.8K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/1qh6ppVtiFTKMyta0NXsjf">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb47267b6a4e1980ad9a29887a" class="card-icon">
<div class="card-content">
<div class="card-title">Madball</div>
<div class="card-description">Artist · 124.4K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/3zDvanHxaETiHltPkKKYhT">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebdb38737b46af3f590641f47f" class="card-icon">
<div class="card-content">
<div class="card-title">Agnostic Front</div>
<div class="card-description">Artist · 184.2K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/3CfJckVRuukdJSvK3r89yJ">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5ebbe8e49363f43d1b5fd16f5bb" class="card-icon">
<div class="card-content">
<div class="card-title">Raimundos</div>
<div class="card-description">Artist · 2.6M monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/0Aj4m8El9TdnqyVHhkuloa">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb8a43d5c1fff3079c2d4a68f9" class="card-icon">
<div class="card-content">
<div class="card-title">Bia Ferreira</div>
<div class="card-description">Artist · 84.9K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/6wd8OZcCaRQNDIMz6SPNGN">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb111e1c28a58df0e4c79b6a40" class="card-icon">
<div class="card-content">
<div class="card-title">MULAMBA</div>
<div class="card-description">Artist · 45.1K monthly listeners.</div>
</div>
</div>
</a>
\n\n
<a target="_blank" rel="noopener noreferrer" class="card-link" href="https://open.spotify.com/artist/7rQQ0SIloZjBvKkWluoiaB">
<div class="card">
<img src="https://i.scdn.co/image/ab6761610000e5eb1d9fe2abf49e43ae0f27585a" class="card-icon">
<div class="card-content">
<div class="card-title">Drope Soviético</div>
<div class="card-description">Artist · 120 monthly listeners.</div>
</div>
</div>
</a>
\n\n
\n\n
