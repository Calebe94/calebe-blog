<!--
title: "Treino de 23/12/21 à 28/02/22"
subtitle: ""
date: "2022-01-10 20:41:26"
tags: []
-->
# Treino de 23/12/21 à 28/02/22

## Treino (A1) - Costas

| Exercício | Série | Repetição| Como fazer |
|:---------:|:-----:|:--------:|:----------:|
|  Pulley     |   4     |  15            | [<img src="https://media.calebe.dev.br/images/pulley-frente-puxador-.jpg" width="200">](https://www.youtube.com/watch?v=4BAnHCJ1K44) |
|  Remada Máquina Articulado | 4 | 15    | [<img src="https://media.calebe.dev.br/images/remada-na-m%C3%A1quina-articulada-e-musculos-recrutados.jpg" width="200">](https://www.youtube.com/watch?v=cwZi6M76s3Q) |
|  Barra Fixa Graviton | 3 | 15 | [<img src="https://media.calebe.dev.br/images/na-maquina-auxilio.jpg" width="200">](https://www.youtube.com/watch?v=79J_0_hy3zU) |
|  Remada Curvada Cross | 4 | 12 | [<img src="https://media.calebe.dev.br/images/remada-curvada-cross.jpg" width="200">](https://www.youtube.com/watch?v=OG_ATPGt4Rc) |

## Treino (A2)

| Exercício | Série | Repetição | Como fazer |
|:---------:|:-----:|:---------:|:----------:|
|  Rosca Direta Livre      | 4 | 10 | [<img src="https://media.calebe.dev.br/images/rosca-direta.jpg" width="200">](https://www.youtube.com/watch?v=s4B8UW3BMqk) |
|  Rosca Scott Máquina     | 3 | 12 | [<img src="https://media.calebe.dev.br/images/rosca-scott-maquina.jpg" width="200">](https://www.youtube.com/watch?v=yIIgcVRkzus) |
|  Rosca Alternada         | 4 | 10 | [<img src="https://media.calebe.dev.br/images/rosca-alternada.jpg" width="200">](https://www.youtube.com/watch?v=kIK3WNjh6zE) |

## Treino (B1)

### Quadríceps

| Exercício | Série | Repetição | Como fazer |
|:---------:|:-----:|:---------:|:----------:|
| Agachamento Smith | 4 | 10 | [<img src="https://media.calebe.dev.br/images/agachamento-livre-ou-agachamento-no-smith.jpg" width="200">](https://www.youtube.com/watch?v=SC-PodU3G64) |
| Leg press | 4 | 12 | [<img src="https://media.calebe.dev.br/images/estensao-joelhos-leg-press.jpg" width="200">](https://www.youtube.com/watch?v=gWwKpTi_phY) |
| Extensor Isométrico 03" | 4 | 10 | [<img src="https://media.calebe.dev.br/images/extensor.jpg" width="200">](https://www.youtube.com/watch?v=8tCdzdWot20) |

### Outros músculos

| Exercício | Série | Repetição | Como fazer |
|:---------:|:-----:|:---------:|:----------:|
| Flexor sentado | 4 | 12 | [<img src="https://media.calebe.dev.br/images/flexor-sentado.jpg" width="200">](https://www.youtube.com/watch?v=vWFHYnVdBp4) |
| Pant. Leg + Tibial Leg | 3 | 20 + 20 | [<img src="https://media.calebe.dev.br/images/panturrilha-leg-press.jpg" width="200">](https://www.youtube.com/watch?v=aPhesY3Z0RI) |
| Abdutor Tronco a Frente | 4 | 15 | [<img src="https://media.calebe.dev.br/images/adutor-tronco-a-frente.jpg" width="200">](https://www.youtube.com/watch?v=rWNuRzi2g1g) |

## Treino Abdome

<div width=100%>
| Exercício | Série | Repetição | Como fazer |
|:---------:|:-----:|:---------:|:----------:|
| Bike | 3 | 20 | [<img src="https://media.calebe.dev.br/images/abdominal-bike.jpg" width="200">](https://www.youtube.com/watch?v=oB6Hn_PaM9U) |
| Remador | 3 | 20 | [<img src="https://media.calebe.dev.br/images/abdominal-remador.jpg" width="200">](https://www.youtube.com/watch?v=Ss5h69C_GFM) |
</div>

## Treino (C1) - Peitoral

| Exercício | Série | Repetição | Como fazer |
|:---------:|:-----:|:---------:|:----------:|
| Supino Reto Barra | 4 | 12 | [<img src="https://media.calebe.dev.br/images/supino-reto.jpg" width="200">](https://www.youtube.com/watch?v=WwXS2TeFmeg) |
| Supino Inclinado Halter | 4 | 12 | [<img src="https://media.calebe.dev.br/images/supino-inclinado-halter.jpg" width="200">](https://www.youtube.com/watch?v=F4Q1g2z8MWM) |
| Crucifixo Máquina | 3 | 15 | [<img src="https://media.calebe.dev.br/images/crucifixo-maquina.jpg" width="200">](https://www.youtube.com/watch?v=O_FwRxa-hJo) |

## Treino (C2) - Ombro

| Exercício | Série | Repetição | Como fazer |
|:---------:|:-----:|:---------:|:----------:|
| Elevação Lateral Biset | 3 | 15 | [<img src="https://media.calebe.dev.br/images/elevacao-lateral.jpg" width="200">](https://www.youtube.com/watch?v=4qDYMU20dzM) |
| Elevação Frontal Biset | 3 | 15 | [<img src="https://media.calebe.dev.br/images/elevacao-frontal.jpg" width="200">](https://www.youtube.com/watch?v=4qDYMU20dzM) |
| Voador Invertido | 4 | 10 | [<img src="https://media.calebe.dev.br/images/voador-invertido.jpg" width="200">](https://www.youtube.com/watch?v=nNESl9ULiwo) |

## Treino (C3) - Tríceps

| Exercício | Série | Repetição | Como fazer |
|:---------:|:-----:|:---------:|:----------:|
| Tríceps Corda Drop | 3 | 10 + 10 | [<img src="https://media.calebe.dev.br/images/triceps-corda.jpg" width="200">](https://www.youtube.com/watch?v=gbnLZto6b0s) |
| Tríceps Francês | 4 | 10 | [<img src="https://media.calebe.dev.br/images/triceps-frances.jpg" width="200">](https://www.youtube.com/watch?v=gB-QMYMlHLs) |

