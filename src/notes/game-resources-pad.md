---
title: "Nota de recursos e ferramentas para criação de jogos"
subtitle: ""date: "2022-05-13 20:39:20"
tags: [tutoriais,ferramentas,tools,ideias]

---

# Ideias para um protótipo de jogo



## **Tema do jogo**



Algumas propostas do tema do jogo.



![]([https://cdn.mos.cms.futurecdn.net/9fDn93XEiqjkFeCAQ4FE6m-970-80.jpg)](https://cdn.mos.cms.futurecdn.net/9fDn93XEiqjkFeCAQ4FE6m-970-80.jpg))



   * Survival (estilo minecraft, coleta de recursos, criar bases, etc);
   * Fazendinha: estilo stardew valley;
   * Life simulator: estilo animal crossing;


Algo com uma mistura destes estilos seria interessante.



## Prioridades pré construção do jogo



Acho que existem algumas pendências pra criar um know-how suficiente pra experiência de desenvolver o jogo ser produtiva. Sendo elas:

    

   *  Conhecimento na engine que seria usada (pelo amor de deus Calebe n inventa de fazer do 0);
   * Conhecimento em pixelart;
   * Conhecimento em produção sonora (tenho alguns conteúdos referentes a isso já voltado pro Linux, vou compartilhar);
   * Como seria um jogo open-source? E se a gente deixar uma API pra mods ao invés de ser full open source? Ou ambos também. Uma API pra mods deixaria mais user friendly pros devs brincarem.


# Definições do Jogo



   * Plataformas: primariamente PC (não penso em nada mobile e nem para consoles)
   * Gráfico: 2D, Pixelart (8/16 bits), perspectiva top down ou side-scroll
   * Network: offline  (não penso em algo online no momento)
   * Controles: mouse e teclado, joystick(xbox360)
   * Licença: jogo FOSS (Free Software and Open Source).


### Ideias



   * O jogo será uma mistura de Animal Crossing(+ ACNH Happy Home Paradise) e Minecraft;
   * Ou seja, a lore do jogo é aberta para a imaginação. Não sabemos porque o jogador está nesse mundo, só sabemos que ele precisa sobreviver;
   * É semelhante a Animal Crossing pois haverá infinitas ilhas, que o jogador poderá explorar;
   * as ilhas poderão possuir recursos diferentes da ilha no jogador;
   * O jogador poderá customizar todas as ilhas que explorar;
   * O jogador poderá encontrar sobreviventes nessas ilhas, com diferentes características e personalidades;
       * Ilhas com sobreviventes podem ser customizadas pelo jogador, mas não pode ser habitada por ele, pois está sendo habitada por outro sobrevivente;
   * Os sobreviventes podem pedir favores para o jogador;
   * A geração do "terreno" será infinita. Porém, terá um espaço finito para exploração ao exemplo de como os mundos de Minecraft funcionam;
   * Os recursos naturais serão regenerados em /tempos a serem definidos/;
   * O jogador poderá realizar trocas comerciais com os sobreviventes;
       * Os sobreviventes podem ou não aceitar as trocas (aleatório ou dependendo do tipo dos itens - se o sobrevivente estiver em uma ilha de maçãs, ele não aceitará maçãs por exemplo);
   * O jogador poderá craftar novos itens com os itens que adquirir.
       * Exemplo: poderá criar cadeiras, mesas e cercas com madeiras, varas de pesca com gravetos;
   * O "mundo" será feito em "grade"(grid) para facilitar o posicionamento de itens.
       * Os itens terão tamanhos variáveis, variando de 1x1 até 10x10 (tamanhos podem ser discutidos);
   * Para explorar novas ilhas o jogador poderá utilizar algum barco.
       * Todo jogador "spawna" em uma ilha com um barco quebrado, o jogador deverá encontrar recursos na ilha para consertar o barco, para assim poder visitar outras ilhas.


#### Considerações



   * A geração do mapa pode ser procedural e infinita. Mas isto não implica haverá um espaço infinito para exploração, podemos limitar o espaço de exploração para uma grade de 1000x1000 por exemplo;
   * As ilhas poderão possuir um espaço de 100x100 (no ACNH a ilha principal é 80x64 sem contar a praia);
   * O jogo não terá um plot, portanto não terá uma narrativa com início, meio e fim. Logo o jogo não terá um fim ( o fim é a paciência do jogador ).
   * Os recursos naturais do jogo podem não ser regenerados, podendo causar algum impacto na gameplay do jogador. Simulando impactos ambientais no jogo. E também exploração desenfreada de recursos naturais;
       * Uma vez que a ideia do jogo é quase que uma colonização de exploração(Portugueses, espanhóis, belgas e holandeses irão amar esse jogo);
   * Podemos utilizar no jogo, árvores nativas do Brasil, como seringueira, pinus, pau-brasil(raríssima), castanheira, bambu e até mesmo a araucária.


# Recursos

## Ferramentas

### Sprites

   * [https://libresprite.github.io/#](https://libresprite.github.io/#)!/ -** LIBRESPRITE IS A FREE AND OPEN SOURCE PROGRAM FOR CREATING AND ANIMATING YOUR SPRITES.**


### Audio

   * [http://www.zbogucki.com/portfolio/dialogue-generator](http://www.zbogucki.com/portfolio/dialogue-generator) - **Creating Intelligent Gibberish **
   * [https://www.chatmapper.com/](https://www.chatmapper.com/) - **CHATMAPPER IS A FULL FEATURED VISUAL EDITOR FOR MANAGING STORYLINES FOR GAMES.**


### Mapas

   * [https://ldtk.io/release-notes/](https://ldtk.io/release-notes/) - **A modern 2D level editor from the creator of Dead Cells, with a strong focus on user-friendliness.**
   * [https://github.com/eranimo/hexgen](https://github.com/eranimo/hexgen) - **It's a world map generator written in Python.**


### Bibliotecas

   * [https://github.com/ljvmiranda921/seagull](https://github.com/ljvmiranda921/seagull) - **A Python library for Conway's Game of Life**


## Sprites



   * [https://publicdomainvectors.org/pt/vetorial-gratis/Personagem-retr%C3%B4-vector-clip-art/27536.html](https://publicdomainvectors.org/pt/vetorial-gratis/Personagem-retr%C3%B4-vector-clip-art/27536.html)
   * [https://opengameart.org/content/dungeon-crawl-32x32-tiles](https://opengameart.org/content/dungeon-crawl-32x32-tiles)
   * [https://pdpxart.tumblr.com/](https://pdpxart.tumblr.com/)
   * [https://openpixelproject.itch.io/opp2017castle](https://openpixelproject.itch.io/opp2017castle)


## Algoritmos



   * [http://pcg.wikidot.com/pcg-algorithm:cellular-automata](http://pcg.wikidot.com/pcg-algorithm:cellular-automata) - **Cellular Automata**
   * [https://gamedev.stackexchange.com/a/110378](https://gamedev.stackexchange.com/a/110378) - **Smoothing edges on procedural terrain**


## Tutoriais



### Vídeos

   * [https://www.youtube.com/watch?v=nLtD15lfzgw](https://www.youtube.com/watch?v=nLtD15lfzgw) - **Choosing the right tools for game dev | Dev Log #2**
   * [https://www.youtube.com/watch?v=MeMPCSqQ-34](https://www.youtube.com/watch?v=MeMPCSqQ-34) - **Game Development with SDL 2.0 (Steam Dev Days 2014)**
   * [https://www.youtube.com/watch?v=tFsETEP01k8](https://www.youtube.com/watch?v=tFsETEP01k8) - **Aseprite Tutorial For Beginners (Pixel Art)**
   * [https://www.youtube.com/watch?v=REHzpDWUL74](https://www.youtube.com/watch?v=REHzpDWUL74) - **Making Gibberish Text Sounds for your game - Unity Tutorial**
   * [https://www.youtube.com/watch?v=2JCG4fCmeHk](https://www.youtube.com/watch?v=2JCG4fCmeHk) - **Pixel Art Class - Top Down Style Analysis \& Tutorial**
   * [https://www.youtube.com/watch?v=J1sFBDQt8J0](https://www.youtube.com/watch?v=J1sFBDQt8J0) - **The secrets to good Pixel Art animation! (Animation tutorial)**
   * [https://www.youtube.com/watch?v=20KHNA9jTsE](https://www.youtube.com/watch?v=20KHNA9jTsE)** - Why I'm Using Wave Function Collapse for Procedural Terrain | Unity Devlog**
   * [https://www.youtube.com/watch?v=2SuvO4Gi7uY](https://www.youtube.com/watch?v=2SuvO4Gi7uY) - **Superpositions, Sudoku, the Wave Function Collapse algorithm.**


### Artigos

   * [https://saint11.org/blog/pixel-art-tutorials/](https://saint11.org/blog/pixel-art-tutorials/) - **Tutorial de PixelArt do Pedro Medeiros (Monstro sagrado demais)**
   * [https://blog.redbluegames.com/animating-top-down-2d-games-in-unity-5e966b81790e](https://blog.redbluegames.com/animating-top-down-2d-games-in-unity-5e966b81790e) - **Animating Top Down 2D Games in Unity**
   * [https://www.linuxjournal.com/article/8497](https://www.linuxjournal.com/article/8497) - **Embedding Python in Your C Programs**
   * [https://www.geeksforgeeks.org/calling-python-from-c-set-1/](https://www.geeksforgeeks.org/calling-python-from-c-set-1/) - **Calling Python from C | Set 1**
   * [https://prdeving.wordpress.com/2019/05/30/how-to-write-a-game-engine-in-pure-c-part-1-state-manager/](https://prdeving.wordpress.com/2019/05/30/how-to-write-a-game-engine-in-pure-c-part-1-state-manager/) - **How to write a game engine in pure C**


### Dicas

   * [https://www.youtube.com/watch?v=sVVn1C3F87A](https://www.youtube.com/watch?v=sVVn1C3F87A) - **25 Game Dev Tips for Beginners - Tips \& Tricks**


### Livros

   * [http://www.delmarlearning.com/companions/content/1305110382/videos/index.asp?isbn=1305110382](http://www.delmarlearning.com/companions/content/1305110382/videos/index.asp?isbn=1305110382) -** The Black Art of Multiplatform Game Programming**


## Jogos/projetos para inspiração

   * [https://www.html5gamedevs.com/topic/38971-pixel-art-vs-vector-graphics-a-big-deal/](https://www.html5gamedevs.com/topic/38971-pixel-art-vs-vector-graphics-a-big-deal/)