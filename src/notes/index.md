<!--
title: "Anotações"
date: "2022-02-05 18:04:26"
tags: []
-->

# Anotações

* [Ícones não exibidos no Doom Emacs](nerd-fonts-doom-emacs.html);
* [Nota de recursos e ferramentas para criação de jogos](game-resources-pad.html);
* [Treino de 23/03/22 à 30/05/22](treino_23-03-22_30-05-22.html);
* [Treino de 23/12/21 à 28/02/22](treino231221-280222.html);
