# Ícones não exibidos no Doom Emacs

## Problema

![](https://cloud.calebe.dev.br/apps/files_sharing/publicpreview/siKyzzsS92p7dmR?file=/&fileId=123710&x=3840&y=2160&a=true)

![](https://cloud.calebe.dev.br/apps/files_sharing/publicpreview/k7R2LLyfHAW6CAX?file=/&fileId=123711&x=3840&y=2160&a=true)

## Solução

`M^x nerd-icons-install-fonts`

* [Icons not Displaying (Linux and MacOS) · Issue #7485 · doomemacs/doomemacs · GitHub](https://github.com/doomemacs/doomemacs/issues/7485);
