<!--
title: "Blog"
date: "2022-02-05 18:04:26"
tags: []
-->

# Blog

Eu costumo escrever algumas coisas a respeito de tecnologia mas também escrevo umas coisas aleatórias.

<a rel="noopener noreferrer" class="card-link" href="./teclado-momo.html">
<div class="card">
<img src="https://cloud.calebe.dev.br/apps/files_sharing/publicpreview/PHCQrPrMKes33CD?file=/&fileId=122489&x=3840&y=2160&a=true" class="card-icon">
<div class="card-content">
<div class="card-title">Minha Jornada de Criação do Teclado Mecânico Momo</div>
<div class="card-description">20 de Setembro de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./terminal-animation.html">
<div class="card">
<img src="https://cloud.calebe.dev.br/apps/files_sharing/publicpreview/7MJpeQLBQQsGH4o?file=/&fileId=122321&x=3840&y=2160&a=true" class="card-icon">
<div class="card-content">
<div class="card-title">Criando uma Animação CSS Estilo Terminal: Um Guia Passo a Passo</div>
<div class="card-description">15 de Setembro de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./tetris-game.html">
<div class="card">
<img src="https://raw.githubusercontent.com/Calebe94/tetris/main/assets/screenshots/game.png" class="card-icon">
<div class="card-content">
<div class="card-title">Desenvolvendo o Clássico Pong em C com a Biblioteca SDL2</div>
<div class="card-description">10 de Setembro de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./tcc-tools.html">
<div class="card">
<img src="https://www.alphafa.com/wp-content/uploads/2018/09/placeholder-square.jpg" class="card-icon">
<div class="card-content">
<div class="card-title">Ferramentas Essenciais para o Desenvolvimento do seu TCC</div>
<div class="card-description">5 de Setembro de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./tcc.html">
<div class="card">
<img src="https://www.alphafa.com/wp-content/uploads/2018/09/placeholder-square.jpg" class="card-icon">
<div class="card-content">
<div class="card-title">Desvendando a Eficiência das Técnicas de Compressão de Dados em Dispositivos IoT</div>
<div class="card-description">5 de Setembro de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./pong-em-c-sdl2.html">
<div class="card">
<img src="https://raw.githubusercontent.com/Calebe94/sdl2-pong/main/.screenshots/start1.png" class="card-icon">
<div class="card-content">
<div class="card-title">Desenvolvendo o Clássico Pong em C com a Biblioteca SDL2</div>
<div class="card-description">21 de Agosto de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./teclado-appa.html">
<div class="card">
<img src="https://raw.githubusercontent.com/Calebe94/appa-firmware/main/res/appa.jpeg" class="card-icon">
<div class="card-content">
<div class="card-title">Appa: Teclado 40% ortholinear inspirado no plaid</div>
<div class="card-description">26 de Fevereiro de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./teclado-appa.html">
<div class="card">
<img src="https://cdn.osxdaily.com/wp-content/uploads/2016/09/view-folder-tree-mac-command-line-2.jpg" class="card-icon">
<div class="card-content">
<div class="card-title">Organização de projeto em C</div>
<div class="card-description">23 de Fevereiro de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./construindo-um-teclado-mecanico.html">
<div class="card">
<img src="https://cloud.calebe.dev.br/apps/files_sharing/publicpreview/DJecLYTt3648Zp8?file=/&fileId=80862&x=200&y=200&a=true" class="card-icon">
<div class="card-content">
<div class="card-title">Construindo seu próprio teclado mecânico do zero</div>
<div class="card-description">23 de Fevereiro de 2023</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./migrando-do-gitlab-para-o-github.html">
<div class="card">
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Frtcamp.com%2Fwp-content%2Fuploads%2Fsites%2F2%2F2019%2F02%2FGitlab-Github-migration.png&f=1&nofb=1&ipt=3c2dbc86610f42609d1915175564468dda5469e0271c97faa7d699f9517d070b&ipo=images" class="card-icon">
<div class="card-content">
<div class="card-title">Migrando repositórios do Gitlab para o Github</div>
<div class="card-description">30 de Maio de 2022</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./pong.html">
<div class="card">
<img src="https://git.calebe.dev.br/sdl2-pong/about/.screenshots/start.png" class="card-icon">
<div class="card-content">
<div class="card-title">Meu primeiro jogo em C</div>
<div class="card-description">19 de Maio de 2022</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./Que-tal-criar-um-jogo.html">
<div class="card">
<img src="https://wiki.libsdl.org/static_files/logo.png" class="card-icon">
<div class="card-content">
<div class="card-title">Que tal criar um jogo?</div>
<div class="card-description">9 de Maio de 2022</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./servidor-de-email-privado.html">
<div class="card">
<img src="https://www.alphafa.com/wp-content/uploads/2018/09/placeholder-square.jpg" class="card-icon">
<div class="card-content">
<div class="card-title">Servidor de email (self-hosted)</div>
<div class="card-description">1 de março de 2022</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./em-busca-de-uma-interface-git.html">
<div class="card">
<img src="https://www.flexmind.co/wp-content/uploads/2020/04/logo-git-icon-e1586889963495.png" class="card-icon">
<div class="card-content">
<div class="card-title">Em busca de uma interface git self-hosted</div>
<div class="card-description">28 de fevereiro de 2022</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./fix_zsh_corrupt_history.html">
<div class="card">
<img src="https://p.e-words.jp/img/zsh.png" class="card-icon">
<div class="card-content">
<div class="card-title">Resolvendo historico corrompido do ZSH</div>
<div class="card-description">17 de abril de 2021</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./premissas_basicas.html">
<div class="card">
<img src="http://lh3.ggpht.com/_6p3hNkUNWrQ/SjpEiMoM3TI/AAAAAAAABdE/9lkeDQLzXUY/s800/bg2009061801.gif" class="card-icon">
<div class="card-content">
<div class="card-title">Premissas básicas de desenvolvimento - Suckless</div>
<div class="card-description">17 de abril de 2021</div>
</div>
</div>
</a>

<a rel="noopener noreferrer" class="card-link" href="./ESPBoy-A-ESP32-Gameboy.html">
<div class="card">
<img src="https://i.imgur.com/mkLqXRc.jpg" class="card-icon">
<div class="card-content">
<div class="card-title">ESPBoy: Um Gameboy com o ESP32</div>
<div class="card-description">3 de março de 2019</div>
</div>
</div>
</a>
