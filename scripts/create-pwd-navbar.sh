#!/bin/bash

#Copyright (C) 2023 Edimar Calebe Castanho

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies:
# * tyaml

usage()
{
    echo 'usage: create-pwd-navbar.sh {PATH}'
    echo 'e.g.: create-pwd-navbar.sh site/'
    exit "$1"
}

[ -z "$1" ] && usage -1

src_dir="$1"

find "$1" -type f -name "*.html" \
   -print0 | while IFS= read -r -d '' file; do
        # remove dirname from path and assert its result to virtual_file_path
        # since it is a virtual path, not a real one
        virtual_file_path="$(echo "$file" | sed "s|$src_dir|/home/|g")"
        # remove extra slashes from virtual path
        virtual_file_path="$(readlink -m "$virtual_file_path")"

        # get post path (if it has any)
        title="$(tyaml "$file" -v title)"

        navbar="$(python scripts/pwd-navbar.py "$virtual_file_path" "$title")"
        sed -i "s|__PWD__|$navbar|g" "$file"
    done
