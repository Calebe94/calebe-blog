#!/usr/bin/python3

#Copyright (C) 2023 Edimar Calebe Castanho

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os

def create_pwd_nav(filename, title):
    aux_navigation_items = filename.split("/")
    navigation_items = list(filter(None, aux_navigation_items))

    navbar = '<h5>' \
    '<div class="terminal__prompt--pwd">$' \
        '<span class="terminal__prompt--typing">' \
            '<div class="cover cover--pwd"></div> pwd' \
        '</span>' \
    '</div>' \
    '<div class="terminal__prompt--pwd-return">' \
        '<span>/'
    path = "/"
    for nav_item in navigation_items:
        if nav_item == "home":
            navbar+='<a href="/">{}</a>/'.format(nav_item)
        elif ".html" in nav_item:
            if "index.html" in nav_item:
                continue
            path=os.path.join(path, nav_item)
            navbar+='<a href="{}">{}</a>'.format(path, title if title else nav_item)
        else:
            path=os.path.join(path, nav_item)
            navbar+='<a href="{}">{}</a>/'.format(path,nav_item)
    navbar += '</span>' \
        '</div>' \
        '</h5>'
    print(navbar)

def main():
    if len(sys.argv) > 1:
        try:
            create_pwd_nav(sys.argv[1], sys.argv[2])
        except Exception as e:
            print(e)

if __name__ == '__main__':
    main()
