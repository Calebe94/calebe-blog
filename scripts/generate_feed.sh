#!/bin/bash

#Copyright (C) 2022 Edimar Calebe Castanho

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies:
# * pandoc
# * tyaml

generate_header()
{
    echo '<?xml version="1.0" encoding="utf-8"?>'
    echo '<feed xmlns="http://www.w3.org/2005/Atom">'
	echo -e "\t<title>$title</title>"
	echo -e "\t<link href=\"$domain\" />"
	echo -e "\t<link rel=\"self\" href=\"$domain/feed.xml\" />"
	echo -e "\t<icon>$icon</icon>"
    echo -e "\t<updated>$(date +"%FT%T%:z")</updated>"
	echo -e "\t<id>$domain</id>"
	echo -e "\t<generator>sbs</generator>"
}

create_entry()
{
    post_file="$1"
    post_title="$(tyaml "$1" -v title)"
    post_link="https://blog.calebe.dev.br/posts/$(basename "$1")"
    post_link="${post_link%.*}.html"
    post_datetime=$(tyaml "$1" -v date)
    post_date=$(date -d "$post_datetime" +"%FT%T%:z")

    echo -e "
        <entry>
            <title>$post_title</title>
            <author><name>Edimar Calebe Castanho</name></author>
            <link href=\"$post_link\" />
            <id>$post_link</id>
            <updated>$post_date</updated>
            <content type=\"html\"><![CDATA[
                $(sed '1,6d' "$1" | pandoc -t html)
            ]]></content>
        </entry>
    "
}

generate_footer()
{
    echo '</feed>'
}

usage()
{
    echo "usage generate_feed.sh {OPTIONS} {POSTS}"
    echo 'e.g.: generate_feed.sh -t "Title" -d "http://doma.in" -i "/favicon.ico" src/posts/'
    echo '"-t", "--title", "title" "<title>": add title to feed metadata;'
    echo '"-d", "--domain", "domain" "<domain>": add domain info to feed metadata;'
    echo '"-i", "--icon", "icon" "<icon>": add favicon path to feed metadata;'
    echo '"-h", "--help", "help": show this usage message.'
    exit $1
}

[ -z "$1" ] && usage

parse_arguments()
{
    while [ "$#" -gt 0 ];do
        case "$1" in
            -t | --title | title)
                title="$2"
                shift
                shift
                ;;
            -d | --domain | domain)
                domain="$2"
                shift
                shift
                ;;
            -i | --icon | icon)
                icon="$2"
                shift
                shift
                ;;
            -h | --help | help)
                usage 0
                shift
                ;;
            *)
                path="$1";
                shift
                ;;
        esac
    done
}

parse_arguments "$@"

if [ -z "$title" ] || [ -z "$domain" ] || [ -z "$icon" ] || [ -z "$path" ]; then
    usage -1
fi

generate_header

find "$path" -type f -name "*.md" ! -name "index.md" \
  -print0 | while IFS= read -r -d '' file; do
    create_entry "$file"
  done

generate_footer
