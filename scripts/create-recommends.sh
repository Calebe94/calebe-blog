#!/bin/sh

if [ $# -ne 1 ]; then
  echo "Usage: $0 <src path>"
  exit 1
fi

src_path="$1"
channels_html_file="$src_path/recommends/channels.md"
artists_html_file="$src_path/recommends/artists.md"

get_description()
{
    grep '<meta property="og:description" content=".*"' | sed 's/<meta property="og:description" content="//g' | sed 's/">//g' | sed 's/"\/>//g' | sed 's|&quot;|\"|g'
}

get_title()
{
    grep '<meta property="og:title" content=".*"' | sed 's/<meta property="og:title" content="//g' | sed 's/">//g' | sed 's/"\/>//g' | sed 's|&quot;|\"|g'
}

get_image()
{
    grep '<meta property="og:image" content=".*"' | sed 's/<meta property="og:image" content="//g' | sed 's/">//g' | sed 's/"\/>//g' | sed 's|&quot;|\"|g'
}

append_to_file()
{
    string_to_append="$1"
    file_to_append="$2"
    printf "%s\n" "$string_to_append" >> "$file_to_append"
}

create_post_metadata()
{
    title="$1"
    post_file="$2"

cat << EOF > "$post_file"
<!--
title: "$title"
subtitle: ""
date: "$(date +"%F")"
tags: []

-->
EOF
}

youtube_channels()
{
    [ -f "$channels_html_file" ] && rm "$channels_html_file"
    create_post_metadata "Canais de Youtube" "$channels_html_file"
    echo "creating youtube recommendation list..."
    append_to_file "## Canais de Youtube\n\n" "$channels_html_file"
    while read -r channel; do
        about="$(curl -s "$channel"/about | shup -r 'meta')"
        description="$(echo "$about" | get_description)"
        title="$(echo "$about" | get_title)"
        image="$(echo "$about" | get_image)"

        echo "creating entry for $title - $channel..."

        append_to_file "<a target=\"_blank\" rel=\"noopener noreferrer\" class=\"card-link\" href=\"$channel\">" "$channels_html_file"
        append_to_file '<div class="card">' "$channels_html_file"
        append_to_file "<img src=\"$image\" class=\"card-icon\">" "$channels_html_file"
        append_to_file '<div class="card-content">' "$channels_html_file"
        append_to_file "<div class=\"card-title\">$title</div>" "$channels_html_file"
        append_to_file "<div class=\"card-description\">$description</div>" "$channels_html_file"
        append_to_file '</div>' "$channels_html_file"
        append_to_file '</div>' "$channels_html_file"
        append_to_file "</a>" "$channels_html_file"
        append_to_file "\n\n" "$channels_html_file"
    done < "$src_path"/recommends/youtube-channels.txt

    append_to_file "\n\n" "$channels_html_file"

    echo "done creating youtube recommendations!"
}

artists_recommendation()
{
    [ -f "$artists_html_file" ] && rm "$artists_html_file"
    create_post_metadata "Artistas / Bandas" "$artists_html_file"
    echo "creating artists recommendation list..."
    append_to_file "## Artistas\n\n" "$artists_html_file"

    while read -r artist; do
        about="$(curl -s "$artist" | shup -r 'meta')"
        description="$(echo "$about" | get_description)"
        title="$(echo "$about" | get_title)"
        image="$(echo "$about" | get_image)"

        echo "creating entry for $title - $artist..."

        append_to_file "<a target=\"_blank\" rel=\"noopener noreferrer\" class=\"card-link\" href=\"$artist\">" "$artists_html_file"
        append_to_file '<div class="card">' "$artists_html_file"
        append_to_file "<img src=\"$image\" class=\"card-icon\">" "$artists_html_file"
        append_to_file '<div class="card-content">' "$artists_html_file"
        append_to_file "<div class=\"card-title\">$title</div>" "$artists_html_file"
        append_to_file "<div class=\"card-description\">$description</div>" "$artists_html_file"
        append_to_file '</div>' "$artists_html_file"
        append_to_file '</div>' "$artists_html_file"
        append_to_file "</a>" "$artists_html_file"
        append_to_file "\n\n" "$artists_html_file"
    done < "$src_path"/recommends/artists.txt

    append_to_file "\n\n" "$artists_html_file"

    echo "done creating artists recommendations!"
}

create_recommends_markdown()
{
    echo "creating recommends.md metatags..."
    echo "<!--" > "$src_path"/recommends.md
    echo "title: \"Minhas recomendações\"" >> "$src_path"/recommends.md
    echo "subtitle: \"Minhas recomendações\"" >> "$src_path"/recommends.md
    echo "date: \"2023-03-17 16:00:00\"" >> "$src_path"/recommends.md
    echo "tags: [PT_BR]" >> "$src_path"/recommends.md
    echo "-->" >> "$src_path"/recommends.md

    printf "# Minhas recomendações\n\n" >> "$src_path"/recommends.md
    echo "done!"
}

create_toc()
{
    echo "creating recommends.md table of content..."
cat << EOF >> "$src_path"/recommends.md
<h2>Índice</h2>
<div>
<ul>
<li><a href="#minhas-recomendações">Minhas recomendações</a>
<ul>
<li><a href="#canais-de-youtube">Canais de Youtube</a></li>
<li><a href="#artistas">Artistas</a></li>
</ul>
</li>
</ul>
EOF
    echo "done!"
}

# create_recommends_markdown
# create_toc
youtube_channels
artists_recommendation

echo "All done!"
