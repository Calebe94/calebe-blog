#!/bin/bash
#Copyright (C) 2022 Edimar Calebe Castanho

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies:
# * tyaml

generate_index()
{
    path="$(dirname $1)"
    md_file="$1"
    if [ -f $md_file ] && [ "$(basename $md_file)" != "index.md" ]; then
        title=$(tyaml $md_file -v title)
        date=$(tyaml $md_file -v date)
        echo "* [$title]($(basename ${md_file%.*}.html))" >> $path/aux_index.txt
    fi
}

usage()
{
    echo 'usage: generate_index.sh {PATH}'
    echo 'e.g.: generate_index.sh src/posts'
    exit $1
}

[ -z "$1" ] && usage -1
find "$1" -type f -name "*.md" ! -name "index.md" \
   -print0 | while IFS= read -r -d '' file; do
        printf '%s\n' "$file"
        cat $(dirname $file)/index.txt > $(dirname $file)/aux_index.txt
        generate_index $file
        mv $(dirname $file)/aux_index.txt $(dirname $file)/index.md
    done

