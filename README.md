![](https://gitlab.com/Calebe94/calebe-blog/badges/main/pipeline.svg)

# ssg5-blog

Blog criado utilizando o gerador de sites estáticos [ssg5](https://www.romanzolotarev.com/ssg.html).

* **Não utilizar o lowdown**, usar o [Markdown.pl](https://daringfireball.net/projects/markdown/). Copiar o arquivo `Markdown.pl` para `/usr/local/bin/`.

## Setup

Para gerar o site preencha as informações no arquivo `Makefile`:

```
ssg5_path=~/.local/bin/ssg5
title=
domain=
src_folder=src/
dst_folder=dst

folder=
ssh=
```

E rodar: `make all`.

No meu caso o `make` poderá ser executado da seguinte forma: 

```
make title=Calebe94 domain=https://calebe.dev.br/site folder=/var/www/my_webapp/www/ ssh=calebe@calebe.dev.br all
```

# Desenvolvedor

| <img src="https://github.com/Calebe94.png?size=200" alt="Edimar Calebe Castanho"> | 
|:---------------------------------------------------------------------------------:|
| [Edimar Calebe Castanho (Calebe94)](https://github.com/Calebe94)                  |
