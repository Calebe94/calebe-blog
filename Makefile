
ssg5_path=ssg5
title="Calebe94"
domain="https://blog.calebe.dev.br"
favicon=static/favicon.ico
src_folder=src/
dst_folder=site/
feed=${dst_folder}/feed.xml

folder=/var/www/site/
ssh=test@test.local

${dst_folder}:
	@echo "Creating dst directory"
	mkdir -p ${dst_folder}

all: deploy

create-index:
	@echo "creating indexes..."
	./scripts/generate_index.sh src/posts/
	./scripts/generate_index.sh src/links/
	./scripts/generate_index.sh src/notes/
	@echo "prebuild done!"

create-recommends:
	@echo "creating recommends page..."
	./scripts/create-recommends.sh src/
	@echo "done!"

prebuild: create-recommends
	@echo "prebuild done!"

postbuild:
	@echo "postbuild"

debug:
	python -m http.server -d ${dst_folder}

build_dbg: ${dst_folder}
	sed -i 's+http://calebe.dev.br/site+http://localhost:8000/site+g' src/_header.html
	${ssg5_path} ${src_folder} ${dst_folder} ${title} http://0.0.0.0:8000
	cp -r static/ ${dst_folder}

build: ${dst_folder} feed
	# sed -i 's+http://localhost:8000/site+http://calebe.dev.br/site+g' src/_header.html
	${ssg5_path} ${src_folder} ${dst_folder} "${title}" "${domain}"
	cp -r static/ ${dst_folder}
	./scripts/create-pwd-navbar.sh ${dst_folder}


deploy: build
	scp -r ${dst_folder} ${ssh}:${folder}

feed: ${dst_folder}
	@echo "Generating Atom feed..."
	@./scripts/generate_feed.sh --title "${title}" --domain "${domain}" --icon "${favicon}" src/posts | tee "${feed}"
	@echo "done!"

.PHONY: all build deploy debug build_dbg prebuild postbuild
